// Hacemos que las variables que hayamos escrito en el fichero .env esten disponibles
// en la lista de variables de entorno.
require('dotenv').config();

const express = require('express');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const cors = require('cors');

// Importamos las rutas.
const userRoutes = require('./src/routes/userRoutes');
const newsRoutes = require('./src/routes/newsRoutes');

// Creamos el servidor.
const app = express();

// Middleware que deserializa un body en formato raw creando la propiedad body en el
// objeto request.
app.use(express.json());

// Middleware que deserializa un body en formato form-data creando la propiedad body y la propiedad files
// en el objeto request.
app.use(fileUpload());

// Middleware que muestra por consola información sobre la petición entrante.
app.use(morgan('dev'));

// Middleware que evita problemas con las CORS cuando intentamos conectar el cliente con
// el servidor.
app.use(cors());

// Middleware que indica al servidor cuál es el directorio de ficheros estáticos.
app.use(express.static(process.env.UPLOADS_DIR));

// Middleware que indica a express donde se encuentran las rutas de los usuarios y los tweets.
app.use(userRoutes);
app.use(newsRoutes);

// Middleware de error.
app.use((err, req, res, next) => {
  console.error(err);

  res.status(err.httpStatus || 500).send({
    status: 'error',
    message: err.message,
  });
});

// Middleware de ruta no encontrada.
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Ruta no encontrada',
  });
});

// Ponemos el servidor a escuchar peticiones en un puerto dado.
app.listen(process.env.PORT, () => {
  console.log(`Server listening at http://localhost:${process.env.PORT}`);
});
