const createNewsQuery = require('../../models/news/createNewsQuery');
const searchCategory = require('../../models/news/searchCategory');
const createNewsSchema = require('../../schemas/createNewsSchema');
const generateError = require('../../services/generateError');
const savePhoto = require('../../services/savePhoto');
const validateSchema = require('../../services/validateSchema');

const createNews = async (req, res, next) => {
  try {
    const { title, leadIn, body, categoryId } = req.body;

    let photo;

    if (req.files?.photo) {
      photo = req.files.photo;
    }

    if (!title || !leadIn || !body || !categoryId) {
      generateError('Faltan campos', 400);
    }

    await validateSchema(createNewsSchema, {
      title,
      leadIn,
      body,
      categoryId,
      photo,
    });

    // Comprobamos que la categoría existe en la base de datos
    await searchCategory(categoryId);

    if (photo) {
      photo = await savePhoto(photo, 0);
    }

    const news = await createNewsQuery(
      title,
      leadIn,
      body,
      categoryId,
      photo,
      req.user.id
    );

    res.send({
      status: 'ok',
      message: 'Noticia generada correctamente',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = createNews;
