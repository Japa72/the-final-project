const selectAllNewsQuery = require('../../models/news/selectAllNewsQuery');

const listNews = async (req, res, next) => {
  try {
    const { categoryId } = req.query;

    // Dado que la propiedad user puede no existir lo indicamos por medio de la interrogación.
    const news = await selectAllNewsQuery(categoryId, req.user?.id);

    res.send({
      status: 'ok',
      data: {
        news,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = listNews;
