const selectUserByEmailQuery = require('../../models/users/selectUserByEmailQuery');
const generateError = require('../../services/generateError');

const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

const loginUser = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      generateError('Faltan campos', 400);
    }

    const user = await selectUserByEmailQuery(email);

    const validPass = await bcrypt.compare(password, user.password);

    if (!validPass) {
      generateError('Contraseña incorrecta', 403);
    }

    const tokenInfo = {
      id: user.id,
    };

    const token = jwt.sign(tokenInfo, process.env.SECRET, {
      expiresIn: '7d',
    });

    if (user.avatar == '') user.avatar = 'avatar.png';

    res.send({
      status: 'ok',
      token,
      user,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = loginUser;
