require('dotenv').config();

const getDB = require('./getDB');

const main = async () => {
  let connection;

  try {
    connection = await getDB();

    await connection.query('CREATE DATABASE IF NOT EXISTS hackabossNews');
    console.log('Creando base de datos');

    console.log('Borrando tablas...');

    await connection.query('DROP TABLE IF EXISTS votes');
    await connection.query('DROP TABLE IF EXISTS news');
    await connection.query('DROP TABLE IF EXISTS categories');
    await connection.query('DROP TABLE IF EXISTS users');

    console.log('Creando tablas...');

    await connection.query(`
            CREATE TABLE IF NOT EXISTS users (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                firstName VARCHAR(30) NOT NULL,
                lastName VARCHAR(30) NOT NULL,
                email VARCHAR(100) UNIQUE NOT NULL,
                password VARCHAR(100) NOT NULL,
                bio TEXT,
                avatar VARCHAR(100),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME
            )
        `);

    await connection.query(`
                CREATE TABLE IF NOT EXISTS categories (
                    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(100) NOT NULL,
                    image VARCHAR(100),
                    createdAt DATETIME NOT NULL
                )
            `);

    await connection.query(`    
            CREATE TABLE IF NOT EXISTS news (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                title VARCHAR(60) NOT NULL,
                leadIn VARCHAR(300) NOT NULL,
                body TEXT NOT NULL,
                categoryId INT UNSIGNED NOT NULL,
                FOREIGN KEY (categoryId) REFERENCES categories(id),
                photo VARCHAR(100),
                userId INT UNSIGNED NOT NULL,
                FOREIGN KEY (userId) REFERENCES users(id),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME
            )    
        `);

    await connection.query(`
            CREATE TABLE IF NOT EXISTS votes (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                likes BOOLEAN,
                userId INT UNSIGNED NOT NULL,
                FOREIGN KEY (userId) REFERENCES users(id),
                newsId INT UNSIGNED NOT NULL,
                FOREIGN KEY (newsId) REFERENCES news(id),
                createdAt DATETIME NOT NULL
            )
        `);

    console.log('¡Tablas creadas!');

    await connection.query(`
            INSERT INTO categories (name, image, createdAt) VALUES
                ('General', '1.jpg', NOW()),
                ('Cultura', '2.jpg', NOW()),
                ('Deportes', '3.jpg', NOW()),
                ('Economía', '4.jpg', NOW()),
                ('Educación', '5.jpg', NOW()),
                ('Medio Ambiente', '6.jpg', NOW()),
                ('Opinión', '7.jpg', NOW()),
                ('Política', '8.jpg', NOW()),
                ('Sociedad', '9.jpg', NOW()),
                ('Sucesos', '10.jpg', NOW()),
                ('Tecnología', '11.jpg', NOW());
    `);

    console.log('¡Categorías generadas!');
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) connection.release();
    process.exit();
  }
};

main();
