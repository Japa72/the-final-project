const jwt = require('jsonwebtoken');

const generateError = require('../services/generateError');

const authUserOptional = async (req, res, next) => {
  try {
    // Obtenemos el token de la cabecera de la petición.
    const { authorization } = req.headers;

    // Si hay creamos la propiedad user en el objeto request.
    if (authorization) {
      let userInfo;

      try {
        userInfo = jwt.verify(authorization, process.env.SECRET);
      } catch {
        generateError('Token incorrecto', 401);
      }

      req.user = userInfo;
    } else req.user = { id: 0 };

    // Saltamos a la siguiente función controladora.
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = authUserOptional;
