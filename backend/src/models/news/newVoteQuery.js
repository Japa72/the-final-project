const getDB = require('../../db/getDB');

const newVoteQuery = async (newsId, likes, userId) => {
  let connection;

  try {
    connection = await getDB();

    // Verificamos si el usuario ya ha votado la publicacion.
    const [votes] = await connection.query(
      `
              SELECT id, likes, newsId, userId, createdAt
              FROM votes WHERE newsId = ? AND userId = ?
            `,
      [newsId, userId]
    );

    if (votes.length > 0) {
      // Si el usuario ya ha emitido un voto, verificamos si la opcion que elige es diferente a la anterior.
      if (likes !== votes[0].likes) {
        // Actualizamos el voto existente con la nueva decision.
        await connection.query(`UPDATE votes SET likes = ? WHERE id = ?`, [
          likes,
          votes[0].id,
        ]);

        // Si el usuario elige la misma opcion, anulamos el voto.
      } else {
        await connection.query(`DELETE FROM votes WHERE id = ?`, [votes[0].id]);
      }

      // Si el usuario no ha votado anadimos el nuevo voto.
    } else {
      await connection.query(
        `INSERT INTO votes (likes, newsId, userId, createdAt) 
         VALUES (?, ?, ?, ?)`,
        [likes, newsId, userId, new Date()]
      );
    }

    // Obtenemos el nuevo total de votos.
    const [news] = await connection.query(
      `
              SELECT
                  CAST(SUM(CASE WHEN V.likes=false THEN 1 ELSE 0 END) AS UNSIGNED) AS negativeVotes,
                  CAST(SUM(CASE WHEN V.likes=true THEN 1 ELSE 0 END) AS UNSIGNED) AS positiveVotes,
                  BIT_OR(IFNULL(V.userId = ? AND V.likes = 1, 0)) AS likedByMe,
                  BIT_OR(IFNULL(V.userId = ? AND V.likes = 0, 0)) AS dislikedByMe

              FROM news N
              LEFT JOIN votes V ON N.id = V.newsId
              WHERE N.id = ?
              GROUP BY N.id
            `,
      [userId, userId, newsId]
    );

    // Retornamos la información actualizada del total de votos.
    return news[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newVoteQuery;
