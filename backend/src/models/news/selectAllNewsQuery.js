const getDB = require('../../db/getDB');

const generateError = require('../../services/generateError');

const selectAllNewsQuery = async (categoryId = '', userId = 0) => {
  let connection;

  try {
    connection = await getDB();

    const [news] = await connection.query(
      `
                SELECT
                    N.id,
                    N.title,
                    N.leadIn,
                    N.body,
                    N.photo,
                    N.categoryId,
                    C.name AS categoryName,
                    U.firstName,
                    U.lastName,
                    N.userId,
                    N.userId = ? AS owner,
                    CAST(SUM(CASE WHEN V.likes=false THEN 1 ELSE 0 END) AS UNSIGNED) AS negativeVotes,
                    CAST(SUM(CASE WHEN V.likes=true THEN 1 ELSE 0 END) AS UNSIGNED) AS positiveVotes,
                    BIT_OR(IFNULL(V.userId = ? AND V.likes = 1, 0)) AS likedByMe,
                    BIT_OR(IFNULL(V.userId = ? AND V.likes = 0, 0)) AS dislikedByMe,
                    N.createdAt
                FROM news N
                INNER JOIN users U ON U.id = N.userId
                LEFT JOIN votes V ON N.id = V.newsId
                INNER JOIN categories C ON N.categoryId = C.id
                WHERE N.categoryId = ? OR ? = ''
                GROUP BY N.id
                ORDER BY N.createdAt DESC
            `,
      [userId, userId, userId, categoryId, categoryId]
    );

    if (news.length < 1) generateError('No hay noticias que mostrar', 404);

    return news;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectAllNewsQuery;
