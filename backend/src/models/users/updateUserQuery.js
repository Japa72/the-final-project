const getDB = require('../../db/getDB');

const updateUserQuery = async (
  firstName,
  lastName,
  email,
  bio,
  avatar,
  userId
) => {
  let connection;

  try {
    connection = await getDB();

    await connection.query(
      `UPDATE users SET firstName = ?, lastName = ?, email = ?,
       bio = ?, avatar = ?, modifiedAt = ? WHERE id = ?`,
      [firstName, lastName, email, bio, avatar, new Date(), userId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateUserQuery;
