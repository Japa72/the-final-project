import './footer.css';

function Footer() {
  return (
    <footer>
      <img src='/LOGO.svg' alt='Hackanews LOGO' />
      <p>&copy; Hack a News 2023</p>
    </footer>
  );
}

export default Footer;
