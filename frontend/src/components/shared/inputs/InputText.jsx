import { string, object, number } from 'prop-types';
import './inputs.css';

function InputText({ label, register, errors, registerName, lengthmax }) {
  return (
    <>
      <label>{label}</label>
      <input type='text' {...register} />
      {errors[registerName]?.type === 'required' && (
        <span className='error'>Campo requerido</span>
      )}
      {errors[registerName]?.type === 'pattern' && (
        <span className='error'>Email inválido</span>
      )}
      {errors[registerName]?.type === 'maxLength' && (
        <span className='error'>{`Máximo ${lengthmax} caracteres`}</span>
      )}
    </>
  );
}

InputText.propTypes = {
  label: string,
  register: object,
  errors: object,
  registerName: string,
  lengthmax: number,
};

export default InputText;
