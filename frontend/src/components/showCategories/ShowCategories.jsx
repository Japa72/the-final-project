// Importamos los hooks.
import { useEffect, useState } from 'react';

// Importamos los componentes.
import { Link } from 'react-router-dom';

// Importamos los servicios.
import { getCategories } from '../../services/newsService';

// Importamos el CSS.
import './showCategories.css';

// Importamos variable de acceso a imagenes de categorías
import { HOSTCAT } from '../../utils/constants';

const ShowCategories = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getCategories()
      .then((response) => {
        setCategories(response.data.data.categories);
      })
      .catch((error) => {
        console.error('Error al obtener las categorías:', error);
      });
  }, []);

  return (
    <div className='categories-container'>
      <ul className='categories-list'>
        {categories.map((category) => (
          <li
            key={category.id}
            style={{ backgroundImage: `url(${HOSTCAT}${category.image})` }}
          >
            <Link to={`/newscategory/${category.id}`}>
              <h2>{category.name}</h2>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ShowCategories;
