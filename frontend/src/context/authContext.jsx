import { useContext, createContext, useState, useEffect } from 'react';
import {
  login,
  signUp,
  updateAvatar,
  updateAccount,
  getCategories,
} from '../services';

import { useNavigate } from 'react-router-dom';
import { CURRENT_USER_LOCAL_STORAGE } from '../utils/constants';

const AuthContext = createContext();

// Accedemos al localStorage para mirar si está el currentUser
const currentUser = JSON.parse(
  localStorage.getItem(CURRENT_USER_LOCAL_STORAGE)
);

export function AuthProvider({ children }) {
  // Variable de estado para guardar los datos del usuario
  // Para después mostrar en el header un "Hola, Ana"
  // Inicializamos con los datos de usuario guardados en el localStorage (si los hay)
  const [user, setUser] = useState(currentUser?.user);
  // Variable de estado para guardar si el usuario está logueado o no
  // Para en el header mostrar o no los links adecuados
  // Inicializamos a true si existe currentUser en el localStorage
  const [isAuthenticated, setIsAuthenticated] = useState(!!currentUser);

  const navigate = useNavigate();

  // carga de la lista de categorías

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getCategories()
      .then((response) => {
        setCategories(response.data.data.categories);
        console.log(categories);
      })
      .catch((error) => {
        console.error('Error al obtener las categorías:', error);
      });
  }, []);

  // Función para loguearse
  const signIn = async (email, password) => {
    try {
      // Llamamos al servicio del login
      const response = await login(email, password);
      // Guardamos que se ha autenticado
      setIsAuthenticated(true);
      // Guardamos los datos de usuario

      setUser(response.data.user);
      localStorage.setItem(
        CURRENT_USER_LOCAL_STORAGE,
        JSON.stringify(response.data)
      );
    } catch (error) {
      // IMPORTANTE: tenemos que retornar de esta manera
      // Sino no llegará el error a la función donde llamamos a ésta
      return Promise.reject(error);
    }
  };

  // Función para registrarse
  const registerUser = async (data) => {
    try {
      // Llamamos al servicio del registro
      const response = await signUp(
        data.firstName,
        data.lastName,
        data.email,
        data.password,
        data.bio
      );
      // Nos logamos con el nuevo usuario
      setIsAuthenticated(true);
      // Guardamos los datos de usuario
      setUser(response.data.user);
    } catch (error) {
      // IMPORTANTE: tenemos que retornar de esta manera
      // Sino no llegará el error a la función donde llamamos a ésta
      return Promise.reject(error);
    }
  };

  // Función para registrarse con avatar
  const registerUserAvatar = async (formData, config) => {
    try {
      // Llamamos al servicio del registro
      const response = await updateAvatar(formData, config);
      // Guardamos que se ha autenticado
      setIsAuthenticated(true);
      // Guardamos los datos de usuario

      user.avatar = response.data.avatar;
      setUser(user);

      currentUser.user.avatar = response.data.avatar;
      localStorage.setItem(
        CURRENT_USER_LOCAL_STORAGE,
        JSON.stringify(currentUser)
      );
    } catch (error) {
      // IMPORTANTE: tenemos que retornar de esta manera
      // Sino no llegará el error a la función donde llamamos a ésta
      return Promise.reject(error);
    }
  };

  const updateProfile = async (formData, config) => {
    try {
      const response = await updateAccount(formData, config);
      user.firstName = response.data.firstName;
      user.lastName = response.data.lastName;
      user.bio = response.data.bio;
      user.email = response.data.email;
      setUser(user);

      currentUser.user.firstName = response.data.firstName;
      currentUser.user.lastName = response.data.lastName;
      currentUser.user.bio = response.data.bio;
      currentUser.user.email = response.data.email;
      localStorage.setItem(
        CURRENT_USER_LOCAL_STORAGE,
        JSON.stringify(currentUser)
      );
    } catch (error) {
      return Promise.reject(error);
    }
  };

  // Función para desloguearse
  const logOut = () => {
    // Eliminamos el currentUser del localStorage
    localStorage.clear(CURRENT_USER_LOCAL_STORAGE);
    // Modifiamos el isAuthenticated a false
    setIsAuthenticated(false);
    // Eliminamos los datos de usuario guardados
    setUser(null);
    // Navegamos al login

    navigate('/');
  };

  return (
    <AuthContext.Provider
      value={{
        signIn,
        registerUser,
        registerUserAvatar,
        updateProfile,
        logOut,
        user,
        isAuthenticated,
        categories,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}
