// Importamos los hooks.
import { useEffect, useState } from 'react';

// Importamos los servicios.
import { getNews } from '../services';

import { useAuth } from '../context/authContext';

const useNews = (categoryId) => {
  const [news, setNews] = useState();

  const { categories } = useAuth();

  const categoryName = categories[categoryId - 1].name;

  useEffect(() => {
    const fetchNews = async () => {
      const response = await getNews(categoryId);

      setNews(response.data.data.news);
    };

    fetchNews();
  }, [categoryId]);

  return { news, categoryName };
};

export default useNews;
