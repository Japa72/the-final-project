import { useEffect, useState } from 'react';
import { getNewsById, voteNews, updateNews, deleteNews } from '../services';
import { useNavigate } from 'react-router-dom';

const useNewsById = (newsId) => {
  const navigate = useNavigate();

  const [selectedNews, setSelectedNews] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchSelectedNews = async () => {
      try {
        setIsLoading(true);

        const response = await getNewsById(newsId);

        setSelectedNews(response.data.data.news);
      } catch (error) {
        console.error('Error al obtener la noticia:', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchSelectedNews();
  }, [newsId]);

  // Función que maneja los likes en la base de datos y el State.
  const handleNewsVotes = async (likes) => {
    // Obtenemos la propiedad data por destructuring para evitar hacer data.data.
    const { data } = await voteNews(likes, newsId);

    // Actualizamos la noticia con el nuevo total de votos.
    setSelectedNews({
      ...selectedNews,
      negativeVotes: data.news.negativeVotes,
      positiveVotes: data.news.positiveVotes,
      likedByMe: data.news.likedByMe,
      dislikedByMe: data.news.dislikedByMe,
    });
  };

  // Función que permite editar una notica en la base de datos y en el State.
  const handleUpdateNews = async (news) => {
    const formData = new FormData();

    formData.append('title', news.title);
    formData.append('leadIn', news.leadIn);
    formData.append('body', news.body);
    formData.append('categoryId', news.categoryId);

    if (news.photo) formData.append('photo', news.photo);

    await updateNews(formData, newsId);

    // Después de editar la noticia redirigimos a la página de la noticia.
    navigate(`/news/${newsId}`);
  };

  //////////////////////////// CONFIRMACION CON POPUP ////////////////////////////

  const [showPopup, setShowPopup] = useState(false);

  const handleConfirmacion = async () => {
    // Lógica para cuando el usuario confirma la acción
    await deleteNews(newsId);
    setShowPopup(false);
    // Redirigimos a la página principal tras eliminar la noticia.
    navigate('/');
  };

  const handleCancelacion = async () => {
    // Lógica para cuando el usuario cancela la acción
    setShowPopup(false);
  };

  ////////////////////////////////////////////////////////////////////////////////

  return {
    selectedNews,
    handleNewsVotes,
    handleUpdateNews,
    isLoading,
    setShowPopup,
    showPopup,
    handleConfirmacion,
    handleCancelacion,
  };
};

export default useNewsById;
