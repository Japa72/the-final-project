import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { createNews } from '../../services';
import { useNavigate } from 'react-router-dom';

function useCreateNews() {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate();

  const [errorPopUp, setErrorPopUp] = useState(false);
  const [selectedPhoto, setSelectedPhoto] = useState();
  const [previewPhoto, setPreviewPhoto] = useState();

  const handlePhotoChange = (e) => {
    const selectedFile = e.target.files[0];
    setSelectedPhoto(selectedFile);
    setPreviewPhoto(URL.createObjectURL(selectedFile));
    setValue('photo', selectedFile);
  };

  const onSubmit = async (data) => {
    try {
      const formData = new FormData();

      formData.append('title', data.title);
      formData.append('leadIn', data.leadIn);
      formData.append('body', data.body);
      formData.append('categoryId', data.categoryId);
      formData.append('photo', selectedPhoto);

      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };

      await createNews(formData, config);

      navigate(`/newscategory/${data.categoryId}`);
    } catch (error) {
      setErrorPopUp(true);
      console.error(error);
    }
  };

  return {
    state: { register, errors, errorPopUp, previewPhoto },
    actions: { handleSubmit, onSubmit, setErrorPopUp, handlePhotoChange },
  };
}

export default useCreateNews;
