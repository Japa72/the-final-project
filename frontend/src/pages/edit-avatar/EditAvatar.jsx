import Block from '../../components/shared/block/Block';
import Button from '../../components/shared/button/Button';
import useEditAvatar from './useEditAvatar';
import avatar from '../../assets/avatar.svg';
import ErrorPopUp from '../../components/shared/error-pop-up/ErrorPopUp';
import { Link } from 'react-router-dom';
import './editavatar.css';

function EditAvatar() {
  const {
    state: { register, errors, errorPopUp, avatarImg },
    actions: { handleSubmit, onSubmit, setErrorPopUp, handleOnChangeAvatar },
  } = useEditAvatar();

  return (
    <>
      <Block text='IMAGEN PERFIL'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='avatar-container'>
            <img src={avatarImg ? avatarImg : avatar} alt='avatar' />
            <input
              className='search'
              type='file'
              {...register('file', {
                required: true,
              })}
              onChange={handleOnChangeAvatar}
            />
          </div>
          {errors.file?.type === 'required' && (
            <span className='error'>Campo requerido</span>
          )}
          <Link to='/profile'>
            <div className='settings'>
              <img src='/settings.svg' alt='user-settings' />
              <p>AJUSTES DE USUARIO</p>
            </div>
          </Link>

          <Button text='ACEPTAR' />
        </form>
      </Block>

      <ErrorPopUp open={errorPopUp} onClose={() => setErrorPopUp(false)} />
    </>
  );
}

export default EditAvatar;
