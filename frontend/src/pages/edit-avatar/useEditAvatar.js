import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../context/authContext';
import { HOST } from '../../utils/constants';

function useEditAvatar() {
  const { registerUserAvatar, user } = useAuth();
  const [errorPopUp, setErrorPopUp] = useState(false);
  // Variable de estado para previsualizar la foto seleccionada
  const [avatarImg, setAvatarImg] = useState(`${HOST}/${user?.avatar}`);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const navigate = useNavigate();

  const onSubmit = async (data) => {
    // Creamos un objeto de formData vacío
    const formData = new FormData();
    // Añadimos uno a uno los campos que necesitamos en backend
    formData.append('avatar', data.file[0]);

    // Agregamos el header para enviar form-data
    const config = {
      header: {
        'Content-Type': 'multipart/form-data',
      },
    };

    try {
      // Llamamos el servicio del signup con los parámetros esperados
      await registerUserAvatar(formData, config);
      // Navegamos al dashboard
      navigate('/');
    } catch (error) {
      // Mostraremos nuestro pop up genérico de error
      setErrorPopUp(true);
    }
  };

  const handleOnChangeAvatar = (e) => {
    const target = e.target.files[0];
    const url = URL.createObjectURL(target);
    setAvatarImg(url);
    console.log(url);
  };

  return {
    state: { register, errors, errorPopUp, avatarImg },
    actions: { handleSubmit, onSubmit, setErrorPopUp, handleOnChangeAvatar },
  };
}

export default useEditAvatar;
