import Block from '../../components/shared/block/Block';
import Button from '../../components/shared/button/Button';

import { useAuth } from '../../context/authContext';

import useNewsById from '../../hooks/useNewsById';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';

const CreateNews = () => {
  const { newsId } = useParams();

  const { selectedNews, handleUpdateNews } = useNewsById(newsId);

  const [title, setTitle] = useState('');
  const [leadIn, setLeadIn] = useState('');
  const [body, setBody] = useState('');
  const [categoryId, setCategoryId] = useState('');
  const [photo, setPhoto] = useState(null);

  const { categories } = useAuth();

  // Actualizamos el valor de las variables de estado cuando obtengamos los datos de la noticia.
  useEffect(() => {
    if (selectedNews) {
      setTitle(selectedNews.title);
      setLeadIn(selectedNews.leadIn);
      setBody(selectedNews.body);
      setCategoryId(selectedNews.categoryId);
    }
  }, [selectedNews]);

  return (
    <>
      <Block text='EDITAR NOTICIA'>
        {selectedNews && (
          <form
            onSubmit={(e) => {
              e.preventDefault();

              handleUpdateNews({
                title,
                leadIn,
                body,
                categoryId,
                photo,
              });
            }}
          >
            <label htmlFor='title'>TITULO</label>
            <input
              type='text'
              id='title'
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />

            <label htmlFor='leadIn'>ENTRADILLA</label>
            <input
              type='text'
              id='leadIn'
              value={leadIn}
              onChange={(e) => setLeadIn(e.target.value)}
            />

            <label htmlFor='newsBody'>CONTENIDO</label>
            <textarea
              id='newsBody'
              value={body}
              onChange={(e) => setBody(e.target.value)}
            />

            <label htmlFor='categoryId'>CATEGORIA</label>
            <select
              value={categoryId}
              id={categoryId}
              className='category'
              onChange={(e) => setCategoryId(e.target.value)}
            >
              {categories.map((category) => {
                return (
                  <option key={category.id} value={category.id}>
                    {category.name}
                  </option>
                );
              })}
            </select>

            <label htmlFor='photo'>FOTO</label>
            <input
              type='file'
              id='photo'
              accept='image/*'
              onChange={(e) => setPhoto(e.target.files[0])}
            />

            <div className='buttonBox'>
              <Button text='EDITAR NOTICIA' />
            </div>
          </form>
        )}
      </Block>
    </>
  );
};

export default CreateNews;
