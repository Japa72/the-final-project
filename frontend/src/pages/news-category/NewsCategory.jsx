import { useParams } from 'react-router-dom';
import useNews from '../../hooks/useNews';
import { HOST } from '../../utils/constants';
import Likes from '../../components/shared/likes/Likes';
import { Link } from 'react-router-dom';
import { formatDate } from '../../utils/date';
import Title from '../../components/shared/title/Title';

// Importamos el CSS.
import './newsCategory.css';

function NewsCategory() {
  // Obtenemos el path param correspondiente.
  const { categoryId } = useParams();

  const { news, categoryName } = useNews(categoryId);

  return news?.length ? (
    <>
      <div className='categoria'>
        <Title text={categoryName} />
      </div>

      {news.map((currentNew) => {
        // Si existe foto nos quedamos con esa foto, de lo contrario establecemos
        // una foto por defecto para las noticias sin foto.
        const photoSrc = currentNew.photo
          ? `http://localhost:8000/${currentNew.photo}`
          : `${HOST}/default-image.jpg`;
        const simpleDate = formatDate(currentNew.createdAt);

        return (
          <div key={currentNew.id} className='noticia'>
            <img src={photoSrc} alt='Imagen' className='image' />
            <Link to={`/news/${currentNew.id}`}>
              <section className='contenido'>
                <p className='titulo'>{currentNew.title}</p>
                <p className='leadIn'>{currentNew.leadIn}</p>
                <p className='autor'>
                  {currentNew.firstName} {currentNew.lastName}
                </p>
                <p className='fecha'>{simpleDate}</p>
                <Likes
                  positive={currentNew.positiveVotes}
                  negative={currentNew.negativeVotes}
                  likedByMe={currentNew.likedByMe}
                  dislikedByMe={currentNew.dislikedByMe}
                />
              </section>
            </Link>
          </div>
        );
      })}
    </>
  ) : (
    <>
      <div className='categoria'>
        <Title text={categoryName} />
      </div>
      <h3>No hay noticias que mostrar</h3>
    </>
  );
}

export default NewsCategory;
