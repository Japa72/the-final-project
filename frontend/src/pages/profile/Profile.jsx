import { EMAIL_REGEX, MAX_LENGTH_STRING } from '../../utils/constants';
import { Link } from 'react-router-dom';
import Block from '../../components/shared/block/Block';
import Button from '../../components/shared/button/Button';
import InputText from '../../components/shared/inputs/InputText';
import InputArea from '../../components/shared/inputs/InputArea';
import useProfile from './useProfile';
import avatar from '../../assets/avatar.svg';
import ErrorPopUp from '../../components/shared/error-pop-up/ErrorPopUp';

import './profile.css';

function Profile() {
  const {
    state: { register, errors, errorPopUp, avatarImg },
    actions: { handleSubmit, onSubmit, setErrorPopUp },
  } = useProfile();

  return (
    <div className='profile-container'>
      <Block text='AJUSTES DE USUARIO'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <ul className='userForm'>
            <li className='avatarContainer'>
              <Link to='/editAvatar'>
                <h3>IMAGEN PERFIL</h3>
                <img
                  className='avatar'
                  src={avatarImg ? avatarImg : avatar}
                  alt='avatar'
                />
              </Link>
            </li>
            <li className='userName'>
              <InputText
                label='NOMBRE'
                register={register('firstName', {
                  required: true,
                  maxLength: MAX_LENGTH_STRING,
                })}
                errors={errors}
                registerName='firstName'
                lengthmax={MAX_LENGTH_STRING}
              />
            </li>
            <li className='userSurname'>
              <InputText
                label='APELLIDO'
                register={register('lastName', {
                  required: true,
                  maxLength: MAX_LENGTH_STRING,
                })}
                errors={errors}
                registerName='lastName'
                lengthmax={MAX_LENGTH_STRING}
              />
            </li>
            <li className='userEmail'>
              <InputText
                label='EMAIL'
                register={register('email', {
                  required: true,
                  pattern: EMAIL_REGEX,
                })}
                errors={errors}
                registerName='email'
              />
            </li>

            <li className='userBio'>
              <InputArea
                label='BIOGRAFIA'
                register={register('bio', {
                  maxLength: 500,
                })}
                errors={errors}
                registerName='bio'
                lengthmax={500}
              />
            </li>
          </ul>
          <div className='button-container'>
            <Button text='ACEPTAR' />
          </div>
        </form>
      </Block>

      <ErrorPopUp open={errorPopUp} onClose={() => setErrorPopUp(false)} />
    </div>
  );
}

export default Profile;
