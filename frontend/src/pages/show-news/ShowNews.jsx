// Importamos los hooks.
import { NavLink, useParams } from 'react-router-dom';
import useNewsById from '../../hooks/useNewsById';
import Popup from 'reactjs-popup';

// Importamos los componentes.
import { Link } from 'react-router-dom';
import Title from '../../components/shared/title/Title';
import Likes from '../../components/shared/likes/Likes';

// Importamos las constantes y las funciones de ayuda.
import { HOST } from '../../utils/constants';
import { formatDate } from '../../utils/date';

// Importamos la imagen que usaremos por defecto si la noticia no tiene imagen propia.
import defaultImage from '../../assets/default-image.jpg';

// Importamos los estilos.
import './showNews.css';

function ShowNews() {
  const { newsId } = useParams();

  const {
    selectedNews,
    handleNewsVotes,
    isLoading,
    setShowPopup,
    showPopup,
    handleConfirmacion,
    handleCancelacion,
  } = useNewsById(newsId);

  if (isLoading) {
    return <p>Cargando...</p>;
  }

  if (!selectedNews) {
    return <p>No se encontró la noticia seleccionada.</p>;
  }

  // Formateamos la fecha.
  const simpleDate = formatDate(selectedNews.createdAt);

  // Seleccionamos la imagen de la noticia.
  const photoUrl = selectedNews.photo
    ? `${HOST}/${selectedNews.photo}`
    : defaultImage;

  return (
    <div className='news'>
      <ul className='newsHeader'>
        <Link to={`/newscategory/${selectedNews.categoryId}`}>
          <li className='newsCategory'>
            <img src='/left.svg' />
            {selectedNews.categoryName}
          </li>
        </Link>
        <li className='newsTitle'>
          <Title text={selectedNews.title} />
        </li>

        <li className='newsLikes'>
          <Likes
            positive={selectedNews.positiveVotes}
            negative={selectedNews.negativeVotes}
            likedByMe={selectedNews.likedByMe}
            dislikedByMe={selectedNews.dislikedByMe}
            handleNewsVotes={handleNewsVotes}
          />
        </li>
      </ul>

      <section>
        <p className='author'>
          {selectedNews.firstName} {selectedNews.lastName} {simpleDate}
        </p>

        <p className='newsLeadIn'>{selectedNews.leadIn}</p>
        <div className='img-container'>
          <img src={photoUrl} alt='Imagen de la noticia' className='photo' />
        </div>
        <p className='newsBody'>{selectedNews.body}</p>
      </section>
      {selectedNews.owner ? (
        <section className='user-actions'>
          <ul>
            <li>
              <NavLink to={`/news/${newsId}/update`}>
                <img
                  src='/edit.svg'
                  alt='Editar noticia'
                  title='Editar'
                  className='edit-icon'
                />
              </NavLink>
            </li>

            <li onClick={() => setShowPopup(true)}>
              <img
                src='/delete.svg'
                alt='Eliminar noticia'
                title='Eliminar'
                className='delete-icon'
              />

              <Popup
                open={showPopup}
                closeOnDocumentClick
                onClose={() => setShowPopup(false)}
              >
                <div>
                  <h3>ELIMINAR NOTICIA</h3>
                  <p>Esta acción es irreversible.</p>
                  <p>¿Estás seguro?</p>
                  <div className='btt-confirm'>
                    <button onClick={handleCancelacion}>CANCELAR</button>
                    <button onClick={handleConfirmacion}>ELIMINAR</button>
                  </div>
                </div>
              </Popup>
            </li>
          </ul>
        </section>
      ) : (
        ' '
      )}
    </div>
  );
}

export default ShowNews;
